from io import FileIO
from makru import CompilingDesc, Sandbox, panic
from makru.helpers import shell
from pathlib import Path

def copy(f1: FileIO, f2: FileIO):
    with f1, f2:
        f2.write(f1.read())

def compile(desc: CompilingDesc):
    if not (Path(desc.root) / "Makefile").exists():
        shell(['./configure'], cwd=desc.root)
    if shell(['make'], cwd=desc.root) == 0:
        header_path = Path(desc.root) / "pth.h"
        include_path = Path(desc.root) / "include"
        if not include_path.exists():
            include_path.mkdir(parents=True)
        new_header_path = include_path / "pth.h"
        copy(open(str(header_path), mode="r"), open(str(new_header_path), mode='w+'))

def test(desc: CompilingDesc):
    shell(['make', 'test'], cwd=desc.root)

def clean(desc: CompilingDesc):
    shell(['make', 'clean'], cwd=desc.root)
    include_path = Path(desc.root) / "include"
    if include_path.exists():
        for p in include_path.glob("*"):
            if not p.is_dir(): p.unlink()
        include_path.rmdir()

def on_sandbox_load(sandbox: Sandbox):
    @sandbox.add_private_action("_asdependency_include")
    def _asdependency_include(desc: CompilingDesc):
        return desc.expand_path("./include")

    @sandbox.add_private_action("_asdependency_info")
    def _asdependency_info(desc: CompilingDesc):
        return {
            'type': desc.type,
            'version': desc.sandbox.config.get("version", None),
        }

    @sandbox.add_private_action("_asdependency_ccflags")
    def _asdependency_ccflags(desc: CompilingDesc):
        return []

    @sandbox.add_private_action("_asdependency_ldflags")
    def _asdependency_ldflags(desc: CompilingDesc):
        return []

    @sandbox.add_private_action("_asdependency_object_files")
    def _asdependency_object_files(desc: CompilingDesc):
        is_static_lib = desc.gvars.get('static-library', False)
        if is_static_lib:
            panic("pth does not support static library")
        return []

    @sandbox.add_private_action("_asdependency_libsearchpaths")
    def _asdependency_libsearchpaths(desc: CompilingDesc):
        is_static_lib = desc.gvars.get('static-library', False)
        if is_static_lib:
            panic("pth does not support static library")
        else:
            return [str(Path(desc.root) / ".libs")]

    @sandbox.add_private_action("_asdependency_libs")
    def _asdependency_libs(desc: CompilingDesc):
        is_static_lib = desc.gvars.get('static-library', False)
        if is_static_lib:
            return []
        else:
            return [desc.name]
